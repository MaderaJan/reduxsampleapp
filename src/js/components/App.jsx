import React from "react";
import List from "./List.jsx";
import Form from "./Form.jsx";
import Post from "./Post.jsx";

const App = () => (
  <>
    <div>
      <h2>Articles</h2>
      <List />
    </div>
    <div>
      <h2>Add a new article</h2>
      <Form />
    </div>
    <div>
      <h2>API posts</h2>
      <Post />
    </div>
  </>
);
export default App;