import React, { useState } from "react";
import { connect } from "react-redux";
import { addArticle } from "../actions/index";

const mapDispatchToProps = (dispatch) => {
  return {
    addArticle: article => dispatch(addArticle(article))
  };
}

const ConnectedFormStateless = () => {
  const [articleTitle, setTitle] = useState({ title: "" })

  const handleChange = (event) => {
    setTitle({ [event.target.id]: event.target.value });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    const { title } = articleTitle;
    addArticle({ title });
    setTitle({ title: "" });
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="title">Title</label>
        <input
          type="text"
          id="title"
          onChange={handleChange}
        />
      </div>
      <button type="submit">SAVE</button>
    </form>
  );
}

const FormStateLess = connect(null, mapDispatchToProps)(ConnectedFormStateless);

export default FormStateLess;