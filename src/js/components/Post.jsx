import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getData } from "../actions/index";
export const ConnectedPost = ({ getData, articles }) => {
    useEffect(() => {
        getData();
    }, [getData])
    return (
        <ul>
            {articles.map(article => (
                <li key={article.id}>{article.title}</li>
            ))}
        </ul>
    );
}

const mapStateToProps = (state) => {
    return {
        articles: state.remoteArticles.slice(0, 10)
    };
}

const Post = connect(mapStateToProps, { getData })(ConnectedPost);

export default Post;