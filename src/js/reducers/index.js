import {
  ADD_ARTICLE,
  FOUND_BAD_WORD,
  DATA_LOADED
} from "../constants/action-types";

const initialState = {
  articles: [],
  remoteArticles: []
};

const rootReducer = (state = initialState, action) => {
  console.log(action.payload);

  switch (action.type) {
    case ADD_ARTICLE:
      return Object.assign({}, state, {
        articles: state.articles.concat(action.payload)
      });

    case FOUND_BAD_WORD:
      return Object.assign({}, state, {
        articles: state.articles.concat(action.payload)
      });

    case DATA_LOADED:
      return Object.assign({}, state, {
        remoteArticles: state.remoteArticles.concat(action.payload)
      });

    default:
      return state;
  }
};

export default rootReducer;
