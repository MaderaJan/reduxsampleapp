import { ADD_ARTICLE, FOUND_BAD_WORD } from "../constants/action-types";

const forbiddenWords = ["penis", "honza"];

export const forbiddenWordsMiddleware = ({ dispatch }) => {
  return next => {
    return action => {
      // do your stuff
      if (action.type === ADD_ARTICLE) {
        const foundWord = forbiddenWords.filter(word =>
          action.payload.title.includes(word)
        );
        if (foundWord.length) {
          return dispatch({ type: FOUND_BAD_WORD, payload: { title: "***" } });
        }
      }
      return next(action);
    };
  };
};
